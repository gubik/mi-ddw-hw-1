package cz.cvut.fit.stefaja5;


import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Kuba on 17. 3. 2015.
 */
public class GateClient {

    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;

    // whether the GATE is initialised
    private static boolean isGateInitilised = false;

    // document to process
    private Document document = null;

    GateClient() {
        initGate();
    }

    public void setDocument(String doc) {
        try {
            this.document = Factory.newDocument(doc);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        if (!isGateInitilised) {
            initGate();
        }

        try {
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

            // locate the JAPE grammar file
            File japeOrigFile = new File("F:/Dropbox/Skola/DDW/HW1/grammar.jape");
            java.net.URI japeURI = japeOrigFile.toURI();

            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }

            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(japeTransducerPR);

            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for (int i = 0; i < corpus.size(); i++) {

                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();

                FeatureMap featureMap = null;
                // get all Token annotations
                AnnotationSet annSetTokens = as_default.get("Email", featureMap);
                System.out.println("Number of Email annotations: " + annSetTokens.size());

                ArrayList tokenAnnotations = new ArrayList(annSetTokens);

                // loop through the Country annotations
                for (int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation) tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    System.out.println("Email: " + underlyingString);
                }

                annSetTokens = as_default.get("Phone", featureMap);
                System.out.println("\nNumber of Phone annotations: " + annSetTokens.size());

                tokenAnnotations = new ArrayList(annSetTokens);

                // loop through the Company annotations
                for (int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation) tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    System.out.println("Phone: " + underlyingString);
                }
            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initGate() {
        try {
            // set GATE home folder
            File gateHomeFile = new File("F:/Programs/GATE_Developer");
            Gate.setGateHome(gateHomeFile);

            // set GATE plugins folder
            File pluginsHome = new File("F:/Programs/GATE_Developer/plugins");
            Gate.setPluginsHome(pluginsHome);

            // initialise the GATE library
            Gate.init();

            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURI().toURL();
            register.registerDirectories(annieHome);

            // flag that GATE was successfully initialised
            isGateInitilised = true;
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
