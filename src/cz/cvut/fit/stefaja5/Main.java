package cz.cvut.fit.stefaja5;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        // Scan input URL
        InputURLScanner urlScan = new InputURLScanner();
        String inputURL = urlScan.scanInputURL();

        try {
            // debug
            //String inputURL = "http://vojtechvesely.cz/";

            Document doc = Jsoup.connect(inputURL).get();
            System.out.println("Fetching: " + inputURL);

            // get all relevant content from the page
            Elements newsHeadlines = doc.select("body");

            // initialize GATE
            GateClient client = new GateClient();

            client.setDocument(newsHeadlines.get(0).text());
            client.run();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
