package cz.cvut.fit.stefaja5;

import java.util.Scanner;

/**
 * Created by Kuba on 17. 3. 2015.
 */
public class InputURLScanner {
    public String scanInputURL() {
        System.out.println("Enter URL to crawl: ");
        String url;

        Scanner scanIn = new Scanner(System.in);
        url = scanIn.nextLine();
        scanIn.close();

        return url;
    }
}
